package com.dsb.deshipos.Activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dsb.deshipos.Database.DBHelper;
import com.dsb.deshipos.Database.DBInfo;
import com.dsb.deshipos.DeshiFragments.LoginFragment;
import com.dsb.deshipos.Library.MySingleton;
import com.dsb.deshipos.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by dynamicsolversbangladesh1 on 3/4/16.
 */
public class ActivationActivity extends Activity {

    EditText editTextActivationCode;
    Button btnSend;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activation_fragment);
        // db helper initialization
        dbHelper=new DBHelper(getApplicationContext());

        editTextActivationCode= (EditText) findViewById(R.id.editTextActivationCode);
        btnSend= (Button) findViewById(R.id.button_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                makeVarify();
            }
        });





    }

    private void makeVarify() {

        final String stringActivationCode=editTextActivationCode.getText().toString();

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID =telephonyManager.getDeviceId();





        //String url="http://mystore1.deshipos.com/api/SyncPos/UpdateDeviceId?key="+stringActivationCode+"&deviceId="+deviceID+"=1";
        //String dumyUrl="http://mystore1.deshipos.com/api/SyncPos/UpdateDeviceId?key=NDPKJ-J4P4T-TU4J7-7RRCK&deviceId=NDPKJ-J4P4T-TU4J7-7RRCK&pair=1";


        String dumyUrl2="http://mystore1.deshipos.com/api/SyncPos/UpdateDeviceId?key="+stringActivationCode+"&deviceId=NDPKJ-J4P4T-TU4J7-7RRCK&pair=1";
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, dumyUrl2, (String)null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String success =response.getString("Success");
                            String message=response.getString("Message");
                            Toast.makeText(getApplicationContext(),success.toString(),Toast.LENGTH_LONG).show();

                            if(success.equals("true")){

                                ContentValues contentValues=new ContentValues(2);
                                contentValues.put("key", "activationCode");
                                contentValues.put("value", stringActivationCode);

                                dbHelper.insert(DBInfo.DATABASE_TABLE_SETTINGS, contentValues);

                               /* if(dbHelper.isTableExists(DBInfo.DATABASE_TABLE_SETTINGS)){

                                    Toast.makeText(getApplicationContext(),DBInfo.DATABASE_TABLE_SETTINGS,Toast.LENGTH_LONG).show();
                                }
                                dbHelper.recordCount(DBInfo.DATABASE_TABLE_SETTINGS, "1=1");

                                String insertedValue=dbHelper.getFieldValue(DBInfo.DATABASE_TABLE_SETTINGS,"1","activationCode");
                                //Toast.makeText(getApplicationContext(),insertedValue.toString(),Toast.LENGTH_LONG).show();

                                long dbs=dbHelper.insert(DBInfo.DATABASE_TABLE_SETTINGS, contentValues);
                                String dbsString=String.valueOf(dbs);
                                Toast.makeText(ActivationActivity.this,dbsString, Toast.LENGTH_SHORT).show();
*/












                                Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
                                startActivity(intent);



                               /* FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                LoginFragment fragment = new LoginFragment();
                                fragmentTransaction.add(R.id.activation_activity, fragment);
                                fragmentTransaction.commit();*/



                            }





                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        //String  msg=response.toString();

                        //Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();

                    }
                });

              MySingleton.getInstance(this).addToRequestQueue(jsObjRequest);


    }
}
