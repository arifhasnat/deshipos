package com.dsb.deshipos.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dsb.deshipos.Database.DBHelper;
import com.dsb.deshipos.Database.DBInfo;
import com.dsb.deshipos.Model.ActivationDataModel;
import com.dsb.deshipos.R;

import java.util.List;


public class WelcomePageActivity extends AppCompatActivity {

    private static final boolean AUTO_HIDE = true;

    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    private static int SPLASH_TIME_OUT = 3000;


    private static final int UI_ANIMATION_DELAY = 300;

    private View mContentView;
    private boolean mVisible;
    DBHelper dbHelper;

    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welcome_page);

        textView= (TextView) findViewById(R.id.textViewDataShow);

        // database helper initialize
        dbHelper=new DBHelper(getApplicationContext());


        actionbarHide();
        mVisible = true;
        mContentView = findViewById(R.id.fullscreen_content);


        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide();
            }
        });

        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, AUTO_HIDE_DELAY_MILLIS);
     }


    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                hide();
            }
            return false;
        }
    };



    private final Handler mHideHandler = new Handler();
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };


    private void hide() {

        if(mVisible){
            mVisible = false;

            int settingsValue=dbHelper.recordCount(DBInfo.DATABASE_TABLE_SETTINGS, "");

            String  settingValueString=String.valueOf(settingsValue);

            Cursor cursor=dbHelper.getResult(DBInfo.DATABASE_TABLE_SETTINGS,"");

            cursor.getColumnIndex("");

            System.out.print(cursor);


            Toast.makeText(WelcomePageActivity.this,settingValueString, Toast.LENGTH_SHORT).show();


            List<ActivationDataModel> activationCode= dbHelper.getDataFromDB();
            ActivationDataModel activationdataModel=new ActivationDataModel();

            textView.setText(activationdataModel.activationCode);

            Toast.makeText(WelcomePageActivity.this,activationCode.toString(), Toast.LENGTH_SHORT).show();

            activationCode.indexOf(activationdataModel.activationCode);
            System.out.println("hgdfhsdddhgdksgfhgbahghsgfsdkgfsd     dfbsdfjsdghfjsdgfdsfsdgfhgdsfghsdgfdshfhsdgfdsjgfdshg"+activationCode.indexOf(activationdataModel.activationCode));






            






           /* if(){


            }
*/
            Intent i = new Intent(WelcomePageActivity.this, ActivationActivity.class);
            startActivity(i);
           /* FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            ActivationFragment fragment = new ActivationFragment();
            fragmentTransaction.add(R.id.welcome_page, fragment);
            fragmentTransaction.commit();*/



        }
    }

    private void actionbarHide() {

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mHideHandler.postDelayed(mHideStatusBar, 0);
    }

    private final Runnable mHideStatusBar = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {

            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
}
