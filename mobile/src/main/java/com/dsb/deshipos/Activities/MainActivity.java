package com.dsb.deshipos.Activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.dsb.deshipos.DeshiFragments.DashboardFragment;
import com.dsb.deshipos.DeshiFragments.LoginFragment;
import com.dsb.deshipos.R;
import com.dsb.deshipos.Utils.Constants;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private CharSequence mTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle = getTitle();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
*/
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setFirstItemNavigationView();
        //onNavigationItemSelected();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

      /*
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setFirstItemNavigationView();
*/
        android.app.Fragment currentFragment = getFragmentManager().findFragmentById(R.id.container);
        if(currentFragment != null) {
            if (currentFragment.getClass() == LoginFragment.class)
                actionbarToggle(false);
            else
                actionbarToggle(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setFirstItemNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.login);
        navigationView.getMenu().performIdentifierAction(R.id.login, 0);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {




        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        //transaction.setCustomAnimations(R.animator.enter_animator, R.animator.exit_animator);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        if (id == R.id.login) {
            transaction
                    .replace(R.id.container, LoginFragment.newInstance("", "")).commit(); //addToBackStack(null).commit();
            // Handle the camera action
        }
          else if  (id == R.id.nav_camara) {
            transaction
                    .replace(R.id.container, DashboardFragment.newInstance("", "")).commit(); //addToBackStack(null).commit();
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void actionbarToggle(boolean showActionBar){
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if(showActionBar)
            {
                if(!actionBar.isShowing())
                    actionBar.show();
            }
            else
            {
                if(actionBar.isShowing())
                    actionBar.hide();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }
       /*
        else {
            super.onBackPressed();
        }
        */
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");
            fm.popBackStackImmediate();
        } else {
            // super.onBackPressed();
            new AlertDialog.Builder(this)
                    .setMessage("Are you sure you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.i("MainActivity", "nothing on backstack, calling super");

                            if (Constants.IS_USER_LOGGEDIN) {
                                Constants.LOGGEDIN_USER = null;
                                Constants.IS_USER_LOGGEDIN = false;
                            }
                            // MainActivity.this.finish();
                            finish();
                            MainActivity.this.finish();

                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }

    }

}
