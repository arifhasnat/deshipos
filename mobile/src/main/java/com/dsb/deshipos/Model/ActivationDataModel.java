package com.dsb.deshipos.Model;

/**
 * Created by dynamicsolversbangladesh1 on 3/7/16.
 */
public class ActivationDataModel  {
    public String activationCode;

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }
}
