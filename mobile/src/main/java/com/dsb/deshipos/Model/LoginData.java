package com.dsb.deshipos.Model;

/**
 * Created by amir on 2/18/2016.
 */
public class LoginData {

    private String AccessToken;

    private String Email;

    private String Id;

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String AccessToken) {
        this.AccessToken = AccessToken;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    @Override
    public String toString() {
        return "ClassPojo [AccessToken = " + AccessToken + ", Email = " + Email + ", Id = " + Id + "]";
    }
}
