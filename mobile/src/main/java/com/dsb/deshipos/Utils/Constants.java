package com.dsb.deshipos.Utils;

import com.dsb.deshipos.ApiModels.Response.LoginResponse;

/**
 * Created by amir on 2/18/2016.
 */
public class Constants {
    public static final String ERRRO_TITLE = "Error";
    public static final String SUCCESS_TITLE = "Successful";
    public static final String PROGRESS_DIALOG_TEXT="Loading...";
    public static final String PROGRESS_DIALOG_TITLE = null;
    // logged in user info
    public static LoginResponse LOGGEDIN_USER;
    public static boolean IS_USER_LOGGEDIN = false;

    public static final String getUserAuthorizationString()
    {
        return  "user " + Constants.LOGGEDIN_USER.getData().getAccessToken();
    }
    public static final String getPublicAuthorizationString()
    {
        return  "public 1234567890";
    }
}
