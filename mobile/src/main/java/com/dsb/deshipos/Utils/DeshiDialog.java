package com.dsb.deshipos.Utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by amir on 2/18/2016.
 */
public class DeshiDialog extends DialogFragment {

    private static String amessage;
    private static DeshiDialog instance = null;
    private static String atitle = "";

    public static DeshiDialog getInstance(String message, String title) {
        amessage = message;
        atitle = title;
        if (instance == null)
            instance = new DeshiDialog();
        return instance;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
        builder.setMessage(amessage)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        instance.dismiss();
                    }
                })
        ;
        builder.setTitle(atitle);
        // Create the AlertDialog object and return it
        return builder.create();
    }
}