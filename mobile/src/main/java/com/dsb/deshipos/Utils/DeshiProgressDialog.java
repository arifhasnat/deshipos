package com.dsb.deshipos.Utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by amir on 2/18/2016.
 */
public class DeshiProgressDialog extends DialogFragment {
    private static String mTitle;
    private static String mMessage;
    private static DeshiProgressDialog instance = null;

    public static DeshiProgressDialog show(FragmentManager fragmentManager,String message, String title) {
        mMessage = message;
        mTitle = title;
        if (instance == null)
            instance = new DeshiProgressDialog();
        instance.mTitle = title;
        instance.mMessage = message;
        instance.show(fragmentManager,null);// (fragmentManager, instance.mMessage, instance.mTitle);
        return instance;
    }
    @Override
    public ProgressDialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setIndeterminateDrawable(null);
        dialog.setTitle(mTitle);
        dialog.setMessage(mMessage);
        dialog.show();
        return dialog;
    }

    public static void hide()
    {
       if(instance !=null)
       {
           instance.dismiss();
       }

    }
}
