package com.dsb.deshipos.DeshiFragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dsb.deshipos.Activities.DashBoard;
import com.dsb.deshipos.ApiModels.Response.LoginResponse;
import com.dsb.deshipos.R;
import com.dsb.deshipos.Utils.ApplicationController;
import com.dsb.deshipos.Utils.Constants;
import com.dsb.deshipos.Utils.DeshiDialog;
import com.dsb.deshipos.Utils.DeshiProgressDialog;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public Context mContext;
    Fragment fragment;


    private OnFragmentInteractionListener mListener;

    private Button btnNumberOne;
    private Button btnNumberTwo;
    private Button btnNumberThree;
    private Button btnNumberFour;
    private Button btnNumberFive;
    private Button btnNumberSix;
    private Button btnNumberSeven;
    private Button btnNumberEight;
    private Button btnNumberNine;
    private Button btnNumberZero;
    private Button btnNumberClear;
    private Button btnNumberGo;

    private TextView tvPinOne;
    private TextView tvPinTwo;
    private TextView tvPinThree;
    private TextView tvPinFour;

    DeshiProgressDialog dpDialog;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragment=getParentFragment();
        View view ;
        view = inflater.inflate(R.layout.fragment_login, container, false);
        btnNumberOne = (Button)view.findViewById(R.id.btnNumberOne);
        btnNumberOne.setOnClickListener(this);
        btnNumberTwo = (Button)view.findViewById(R.id.btnNumberTwo);
        btnNumberTwo.setOnClickListener(this);
        btnNumberThree = (Button)view.findViewById(R.id.btnNumberThree);
        btnNumberThree.setOnClickListener(this);
        btnNumberFour = (Button)view.findViewById(R.id.btnNumberFour);
        btnNumberFour.setOnClickListener(this);
        btnNumberFive = (Button)view.findViewById(R.id.btnNumberFive);
        btnNumberFive.setOnClickListener(this);
        btnNumberSix = (Button)view.findViewById(R.id.btnNumberSix);
        btnNumberSix.setOnClickListener(this);
        btnNumberSeven = (Button)view.findViewById(R.id.btnNumberSeven);
        btnNumberSeven.setOnClickListener(this);
        btnNumberEight = (Button)view.findViewById(R.id.btnNumberEight);
        btnNumberEight.setOnClickListener(this);
        btnNumberNine = (Button)view.findViewById(R.id.btnNumberNine);
        btnNumberNine.setOnClickListener(this);
        btnNumberClear = (Button)view.findViewById(R.id.btnNumberClear);
        btnNumberClear.setOnClickListener(this);
        btnNumberZero = (Button)view.findViewById(R.id.btnNumberZero);
        btnNumberZero.setOnClickListener(this);
        btnNumberGo = (Button)view.findViewById(R.id.btnNumberGo);
        btnNumberGo.setOnClickListener(this);
        tvPinOne = (TextView) view.findViewById(R.id.tvPinOne);
        tvPinTwo = (TextView) view.findViewById(R.id.tvPinTwo);
        tvPinThree = (TextView) view.findViewById(R.id.tvPinThree);
        tvPinFour = (TextView) view.findViewById(R.id.tvPinFour);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
          //  mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnNumberOne)
        {
            SetPinCode("1");
        }
        else if(v.getId()==R.id.btnNumberTwo)
        {
            SetPinCode("2");
        }
        else if(v.getId()==R.id.btnNumberThree)
        {
            SetPinCode("3");
        }
        else if(v.getId()==R.id.btnNumberFour)
        {
            SetPinCode("4");
        }
        else if(v.getId()==R.id.btnNumberFive)
        {
            SetPinCode("5");
        }
        else if(v.getId()==R.id.btnNumberSix)
        {
            SetPinCode("6");
        }
        else if(v.getId()==R.id.btnNumberSeven)
        {
            SetPinCode("7");
        }
        else if(v.getId()==R.id.btnNumberEight)
        {
            SetPinCode("8");
        }
        else if(v.getId()==R.id.btnNumberNine)
        {
            SetPinCode("9");
        }
        else if(v.getId()==R.id.btnNumberZero)
        {
            SetPinCode("0");
        }
        else if(v.getId()==R.id.btnNumberClear)
        {
           ClearPinCode();
        }
        else if(v.getId()==R.id.btnNumberGo)
        {
            String pin_code=GetPinCode();
            if(TextUtils.isEmpty(pin_code) || pin_code.length()<4)
            {
                DeshiDialog.getInstance("Please enter 4 digit pin number",ApplicationController.TAG).show(getFragmentManager(),null);
                return;
            }
            else {

                Intent intent=new Intent(this.getActivity(), DashBoard.class);
                startActivity(intent);
                //makeLoginRequest();
            }

        }
    }
    @TargetApi(Build.VERSION_CODES.M)
    private void makeLoginRequest() {

        dpDialog = DeshiProgressDialog.show(getFragmentManager(),Constants.PROGRESS_DIALOG_TEXT,Constants.PROGRESS_DIALOG_TITLE);//.show(getFragmentManager(),null);

        HashMap<String, String> params = new HashMap<String, String>();

        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID =telephonyManager.getDeviceId();
        params.put("Email", GetPinCode());
        String loginUrl=ApplicationController.API_BASE_URL+ApplicationController.LOGIN+"?key="+GetPinCode()+"&deviceId="+deviceID+"&pair=1";



        JsonObjectRequest req = new JsonObjectRequest(loginUrl, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Gson gson = new Gson();
                            LoginResponse loginResponse = gson.fromJson(response.toString(), LoginResponse.class);
                            Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                            Log.e(ApplicationController.TAG, loginResponse.getMessage());

                            if (TextUtils.equals(loginResponse.getSuccess(), "true")) {
                                Constants.IS_USER_LOGGEDIN = true;
                                Constants.LOGGEDIN_USER = loginResponse;
                                Log.e(ApplicationController.TAG + " log id", Constants.LOGGEDIN_USER.getData().getId());
                                int backStackEntryCount =  getFragmentManager().getBackStackEntryCount();
                                if(backStackEntryCount > 0)
                                  getActivity().onBackPressed(); // this will return previous page and refresh navigation drawer also
                                else
                                {
                                    FragmentManager fragmentManager = getFragmentManager();
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                    transaction
                                            .replace(R.id.container, LoginFragment.newInstance("", "")).addToBackStack(null).commit();
                                }



                            } else {
                                Toast.makeText(getActivity(), loginResponse.getMessage(), Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            Log.e(ApplicationController.TAG +" db" ,e.toString());
                        }
                        dpDialog.hide();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //  String error_message=error.getMessage().toString();
                Log.e(ApplicationController.TAG, error.getMessage().toString());
               Toast.makeText(getActivity(), error.getMessage().toString(), Toast.LENGTH_LONG).show();
              //  hidepDialog();
                dpDialog.hide();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "public 1234567890");
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };


        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(req);
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private void SetPinCode(String pin_code)
    {
      if(TextUtils.isEmpty(tvPinOne.getText().toString().trim())) {
          tvPinOne.setText(pin_code);
          return;
      }
        if(TextUtils.isEmpty(tvPinTwo.getText().toString().trim())) {
            tvPinTwo.setText(pin_code);
            return;
        }
        if(TextUtils.isEmpty(tvPinThree.getText().toString().trim())) {
            tvPinThree.setText(pin_code);
            return;
        }
        if(TextUtils.isEmpty(tvPinFour.getText().toString().trim())) {
            tvPinFour.setText(pin_code);
            return;
        }
    }
    private String GetPinCode()
    {
        String pin_code="";
        pin_code+=tvPinOne.getText().toString().trim();
        pin_code+=tvPinTwo.getText().toString().trim();
        pin_code+=tvPinThree.getText().toString().trim();
        pin_code+=tvPinFour.getText().toString().trim();
        return pin_code;
    }
    private void ClearPinCode()
    {
        if(!TextUtils.isEmpty(tvPinFour.getText().toString().trim())) {
            tvPinFour.setText("");
            return;
        }
        if(!TextUtils.isEmpty(tvPinThree.getText().toString().trim())) {
            tvPinThree.setText("");
            return;
        }
        if(!TextUtils.isEmpty(tvPinTwo.getText().toString().trim())) {
            tvPinTwo.setText("");
            return;
        }
        if(!TextUtils.isEmpty(tvPinOne.getText().toString().trim())) {
            tvPinOne.setText("");
            return;
        }



    }

}
