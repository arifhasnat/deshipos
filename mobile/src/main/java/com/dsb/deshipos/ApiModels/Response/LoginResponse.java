package com.dsb.deshipos.ApiModels.Response;

import com.dsb.deshipos.Model.LoginData;

/**
 * Created by amir on 2/18/2016.
 */
public class LoginResponse {
    private String Message;

    private LoginData Logindata;

    private String Success;

    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }

    public LoginData getData ()
    {
        return Logindata;
    }

    public void setData (LoginData data)
    {
        this.Logindata = data;
    }

    public String getSuccess ()
    {
        return Success;
    }

    public void setSuccess (String Success)
    {
        this.Success = Success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Message = "+Message+", data = "+ Logindata +", Success = "+Success+"]";
    }
}
