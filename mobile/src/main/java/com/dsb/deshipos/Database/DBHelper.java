package com.dsb.deshipos.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.dsb.deshipos.Model.ActivationDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amir on 2/18/2016.
 */
public class DBHelper extends SQLiteOpenHelper{

    //version number to upgrade database version
    //each time if you Add, Edit table, you need to change the
    //version number.
    private static final int DATABASE_VERSION = 4;

    // Database Name
    private static final String DATABASE_NAME = "deshi_pos.db";

    public DBHelper(Context context ) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here

        String CREATE_LOGIN_TRACK_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_LOGIN_CHECK  + "("
                + "id"  + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + "user_id" + " TEXT,"
                + "login_time" + " TEXT)";
        db.execSQL(CREATE_LOGIN_TRACK_TABLE);




        String CREATE_LAST_CATEGORY_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_LAST_CATEGORY  + "("
                + "id"  + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + "user_id" + " TEXT,"
                + "category_id" + " TEXT)";
        db.execSQL(CREATE_LAST_CATEGORY_TABLE);


        //--setting db query start

        String CREATE_SETTINGS_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_SETTINGS  + "("
                + "key"  + " TEXT ,"
                + "value" + " TEXT)";
        db.execSQL(CREATE_SETTINGS_TABLE);


        // --setting db query end


        // --start ----this is the dash board database query string
        // here are all table create statements

        String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_CATEGORIES  + "("
                + "CategoryId"  + " INTEGER PRIMARY KEY ,"
                + "ParentCategoryId" + " INTEGER,"
                + "Name" + " TEXT,"
                + "ImageUrl" + " TEXT,"
                + "ThumbUrl" + " TEXT,"
                + "UpdatedOnUtc" + " TEXT,"
                + "Deleted" + " INTEGER)";
        db.execSQL(CREATE_CATEGORIES_TABLE);

        String CREATE_Customers_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_CUSTOMERS  + "("
                + "CustomerId"  + " INTEGER PRIMARY KEY ,"
                + "CustomerNumber" + " TEXT,"
                + "Barcode" + " TEXT,"
                + "FirstName" + " TEXT,"
                + "LastName" + " TEXT,"
                + "Street" + " TEXT,"
                + "City" + " TEXT,"
                + "State" + " TEXT,"
                + "Zip" + " TEXT,"
                + "Phone" + " TEXT,"
                + "Mobile" + " TEXT,"
                + "Email" + " TEXT,"
                + "Notes" + " TEXT,"
                + "UpdatedOnUtc" + " TEXT,"
                + "Deleted" + " INTEGER)";
        db.execSQL(CREATE_Customers_TABLE);

        String CREATE_Discounts_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_DISCOUNTS  + "("
                + "Id"  + " INTEGER PRIMARY KEY ,"
                + "Title" + " TEXT,"
                + "Amount" + " TEXT,"
                + "Fixed" + " INTEGER,"
                + "DiscountType" + " INTEGER,"
                + "Email" + " TEXT,"
                + "PinNumber" + " TEXT,"
                + "MaxPurchase" + "DOUBLE ,"
                + "MinPurchase" + "DOUBLE,"
                + "ItemLevel" + " INTEGER,"
                + "StartDAte" + "TEXT,"
                + "EndDate" + " TEXT,"
                + "Active" + " INTEGER,"
                + "CategoryId" + "INTEGER,"
                + "SubCategoryId" + "INTEGER,"
                + "CreatedOnUtc" + " TEXT,"
                + "UpdatedOnUtc" + " TEXT,"
                + "Deleted" + " INTEGER)";
        db.execSQL(CREATE_Discounts_TABLE);


        String CREATE_Employees_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_EMPLOYEES  + "("
                + "EmployeeId"  + " INTEGER PRIMARY KEY ,"
                + "PositionId" + " INTEGER,"
                + "FirstName" + " TEXT,"
                + "LastName" + " TEXT,"
                + "Mobile" + " TEXT,"
                + "Email" + " TEXT,"
                + "PinNumber" + " TEXT,"
                + "ExternalID" + " TEXT,"
                + "StartDate" + " TEXT,"
                + "EndDate" + " TEXT,"
                + "Picture" + " TEXT,"
                + "Active" + " TEXT,"
                + "Password" + " TEXT,"
                + "UpdatedOnUtc" + " TEXT,"
                + "Deleted" + " INTEGER)";
        db.execSQL(CREATE_Employees_TABLE);

        String CREATE_Positions_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_POSITIONS  + "("
                + "PositionId"  + " INTEGER PRIMARY KEY ,"
                + "Name" + " TEXT,"
                + "DisplayOrder" + "INTEGER,"
                + "CreatedOnUtc" + " Date,"
                + "UpdatedOnUtc" + " Date,"
                + "Deleted" + " INTEGER)";
        db.execSQL(CREATE_Positions_TABLE);

        String CREATE_PRODUCTS_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_PRODUCTS  + "("
                + "ProductId"  + " INTEGER PRIMARY KEY ,"
                + "ParentProductId" + "INTEGER,"
                + "BaseProductId" + " INTEGER,"
                + "CategoryId" + " INTEGER,"
                + "SubCategoryId" + " INTEGER,"
                + "SKU" + " TEXT,"
                + "Barcode" + " TEXT,"
                + "Name" + "TEXT ,"
                + "NameExt" + "TEXT,"
                + "Description" + " TEXT,"
                + "ImageUrl" + "TEXT,"
                + "ThumbUrl" + " TEXT,"
                + "Cost" + " INTEGER,"
                + "Price" + "INTEGER,"
                + "PriceWithTax" + "INTEGER,"
                + "SalesTaxRequired" + " INTEGER,"
                + "SpecialPrice" + " INTEGER,"
                + "SpecialPriceDateStart" + "TEXT,"
                + "SpecialPriceDateEnd" + "TEXT,"
                + "SpecialPriceTitle" + " TEXT,"
                + "QuantityStock" + " INTEGER,"
                + "StockThreshold" + " INTEGER,"
                + "TrackInventory" + " INTEGER,"
                + "WeightItem" + " INTEGER,"
                + "Variant" + " INTEGER,"
                + "EditPrice" + " INTEGER,"
                + "HasIME" + " INTEGER,"
                + "UnitId" + " INTEGER,"
                + "ManufacturerId" + " INTEGER,"
                + "SupplierId" + " INTEGER,"
                + "DiscountId" + " INTEGER,"

                + "CreatedOnUtc" + " TEXT,"
                + "UpdatedOnUtc" + " TEXT,"
                + "Deleted" + " INTEGER,"

                + "DisplayOrder" + " INTEGER)";
        db.execSQL(CREATE_PRODUCTS_TABLE);


        String CREATE_PRODUCTVARIANTS_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_PRODUCTVARIATIONS  + "("
                + "ProductVariantId"  + " INTEGER PRIMARY KEY ,"
                + "ProductId" + " INTEGER,"
                + "VariantAttributeId" + "INTEGER,"
                + "DisplayOrder" + " INTEGER)";
        db.execSQL(CREATE_PRODUCTVARIANTS_TABLE);

        String CREATE_SALEITEMS_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_SALEITEMS  + "("
                + "SaleItemId"  + " INTEGER PRIMARY KEY ,"
                + "SaleId" + " INTEGER,"
                + "ProductId" + " INTEGER,"
                + "ClientSaleItemId" + " INTEGER,"
                + "Quantity" + " DOUBLE,"
                + "Price" + " DOUBLE,"
                + "Total" + " DOUBLE,"
                + "Description" + "TEXT ,"
                + "IME" + "TEXT,"
                + "ReturnItem" + " INTEGER)";
        db.execSQL(CREATE_SALEITEMS_TABLE);

        String CREATE_SALES_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_SALES  + "("
                + "SaleId"  + " INTEGER PRIMARY KEY ,"
                + "ClientSaleId" + " INTEGER,"
                + "CustomerId" + " INTEGER,"
                + "EmployeeId" + " INTEGER,"
                + "Code" + " TEXT,"
                + "Date" + "TEXT,"
                + "Amount" + " REAL,"
                + "Vat" + "REAL ,"
                + "Discount" + "REAL,"
                + "Cash" + " REAL,"
                + "Card" + " REAL,"
                + "Voucher" + " REAL,"
                + "ReturnAmount" + "REAL,"
                + "ReturnCode" + "TEXT,"
                + "VoucherNumber" + "TEXT,"
                + "Change" + "REAL ,"
                + "Less" + "REAL,"
                + "Other" + "REAL,"
                + "Notes" + "TEXT,"
                + "CreatedOnUtc" + " TEXT,"
                + "UpdatedOnUtc" + " TEXT,"
                + "Deleted" + " INTEGER)";
        db.execSQL(CREATE_SALES_TABLE);

        String CREATE_SUPPLIERS_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_SUPPLIERS  + "("
                + "SupplierId"  + " INTEGER PRIMARY KEY ,"
                + "SupplierNumber" + " TEXT,"

                + "Name" + " TEXT,"
                + "Street" + " TEXT,"
                + "City" + " TEXT,"
                + "State" + " TEXT,"
                + "Zip" + "TEXT ,"
                + "Phone" + "TEXT,"
                + "Mobile" + "TEXT,"
                + "Email" + "TEXT,"
                + "Notes" + "TEXT,"
                + "CreatedOnUtc" + " TEXT,"
                + "UpdatedOnUtc" + " TEXT,"
                + "Deleted" + " INTEGER)";
        db.execSQL(CREATE_SUPPLIERS_TABLE);

        String CREATE_UNITS_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_UNITS  + "("
                + "UnitId"  + " INTEGER PRIMARY KEY ,"
                + "Code" + " TEXT,"
                + "Name" + " TEXT,"
                + "DisplayOrder" + " INTEGER,"
                + "CreatedOnUtc" + " TEXT,"
                + "UpdatedOnUtc" + " TEXT,"
                + "Deleted" + " INTEGER)";
        db.execSQL(CREATE_UNITS_TABLE);

        String CREATE_VARIANT_ATTRIBUTE_OPTIONS_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_VARIANTATTRIBUTEOPTION  + "("
                + "VariantAttributeOptionId"  + " INTEGER PRIMARY KEY ,"
                + "VariantAttributeId" + "INTEGER,"
                + "Name" + " TEXT,"
                + "DisplayOrder" + " INTEGER)";
        db.execSQL(CREATE_VARIANT_ATTRIBUTE_OPTIONS_TABLE);

        String CREATE_VARIANT_ATTRIBUTE_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_VARIANTATTRIBUTES  + "("
                + "VariantAttributeOptionId"  + " INTEGER PRIMARY KEY ,"
                + "VariantAttributeId" + "INTEGER,"
                + "Name" + " TEXT,"
                + "CreatedOnUtc" + " TEXT,"
                + "UpdatedOnUtc" + " TEXT,"
                + "Deleted" + " INTEGER)";
        db.execSQL(CREATE_VARIANT_ATTRIBUTE_TABLE);


        String CREATE_VOUCHERS_TABLE = "CREATE TABLE " + DBInfo.DATABASE_TABLE_VOUCHERS  + "("
                + "Id"  + " INTEGER PRIMARY KEY ,"
                + "Number" + "TEXT,"
                + "Name" + " TEXT,"
                + "Amount" + "REAL,"

                + "StartDate" + "TEXT,"
                + "EndDate" + "TEXT,"
                + "Type" + "INTEGER,"
                + "Status" + "INTEGER,"
                + "EmployeeId" + "INTEGER,"
                + "ReturnId" + "INTEGER,"
                + "ReturnCode" + "TEXT,"
                + "UsedId" + "INTEGER,"
                + "UsedCode" + "TEXT,"
                + "UsedDate" + "TEXT,"
                + "CreatedOnUtc" + "TEXT,"
                + "UpdatedOnUtc" + " TEXT,"
                + "Deleted" + " INTEGER)";
        db.execSQL(CREATE_VOUCHERS_TABLE);




    // --end of dash board databse query string ----









    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_LOGIN_CHECK );

        // this the dashbort db update

        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_CATEGORIES );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_CUSTOMERS );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_DISCOUNTS );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_EMPLOYEES );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_POSITIONS );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_PRODUCTS );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_PRODUCTVARIATIONS );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_SALEITEMS );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_SALES );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_SUPPLIERS );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_UNITS );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_VARIANTATTRIBUTEOPTION );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_VARIANTATTRIBUTES );
        db.execSQL("DROP TABLE IF EXISTS " +  DBInfo.DATABASE_TABLE_VOUCHERS );



        // Create tables again
        onCreate(db);

    }
    public  SQLiteDatabase getWritableDB()
    {
        return getWritableDatabase();
    }
    public boolean isTableExists( String tableName)
    {
        SQLiteDatabase db = getWritableDB();
        if (tableName == null || db == null || !db.isOpen())
        {
            return false;
        }
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[] {"table", tableName});
        if (!cursor.moveToFirst())
        {
            return false;
        }
        int count = cursor.getInt(0);
        cursor.close();
        db.close();
        return count > 0;
    }
    public Cursor getResult(String tableName,String where)
    {
        SQLiteDatabase db = getWritableDB();
        if (tableName == null || db == null || !db.isOpen())
        {
            return null;
        }
        String sqlQuery="SELECT * FROM "+tableName;
        if(!where.isEmpty())
            sqlQuery+=" WHERE "+where;

        Cursor cursor = db.rawQuery(sqlQuery, null);
        // db.close();
        return cursor;
    }
    public String getFieldValue(String tableName,String where,String columnName)
    {
        Cursor cursor = getResult(tableName, where);

        if (cursor == null || !cursor.moveToFirst())
        {
            return "";
        }
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    public int recordCount(String tableName,String where)
    {
        Cursor cursor = getResult(tableName,where);

        if (cursor == null || !cursor.moveToFirst())
        {
            return 0;
        }
        int count = cursor.getCount();
        cursor.close();
        return count;
    }
    public long insert(String table,ContentValues data)
    {
        long insert_id=0;
        SQLiteDatabase db = getWritableDB();
        try{
            insert_id = db.insert(table, null,data);
        }
        catch (Exception e){
            String error =  e.getMessage().toString();
        }
        return insert_id;
    }
    public void update(String table, ContentValues data,String where,String[] whereArgs)
    {
        SQLiteDatabase db = getWritableDB();
        //String where = "id=?";
        //String[] whereArgs = new String[] {String.valueOf(id)};
        try {
            db.update(table, data, where, whereArgs);
        }
        catch (Exception e){
            String error =  e.getMessage().toString();
        }
    }




    public List<ActivationDataModel> getDataFromDB(){
        List<ActivationDataModel> modelList = new ArrayList<ActivationDataModel>();
        String query = "select * from "+DBInfo.DATABASE_TABLE_SETTINGS;

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);

        if (cursor.moveToFirst()){
            do {
                ActivationDataModel model = new ActivationDataModel();
                model.setActivationCode(cursor.getString(0));

                modelList.add(model);
            }while (cursor.moveToNext());
        }


        Log.d("student data", modelList.toString());


        return modelList;
    }


}
