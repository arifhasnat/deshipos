package com.dsb.deshipos.Database;

import java.security.PublicKey;

/**
 * Created by amir on 2/18/2016.
 */
public class DBInfo {
    /**********DATABASE TABLES*****************/
    public static String DATABASE_TABLE_LOGIN_CHECK="login_check";
    public static String DATABASE_TABLE_LAST_CATEGORY="last_category";

    //---start--here are all table name of deshiPOS that will be used to store data from network request

    public static String DATABASE_TABLE_SETTINGS="settings";
    public  static String DATABASE_TABLE_CATEGORIES="categories";
    public static String  DATABASE_TABLE_CUSTOMERS="customers";
    public static String  DATABASE_TABLE_DISCOUNTS="discouns";
    public static String  DATABASE_TABLE_EMPLOYEES="employees";
    public static String  DATABASE_TABLE_POSITIONS="positions";
    public static String  DATABASE_TABLE_PRODUCTS="producsts";
    public static String  DATABASE_TABLE_PRODUCTVARIATIONS="productVariations";
    public static String  DATABASE_TABLE_SALEITEMS="saleItems";
    public static String  DATABASE_TABLE_SALES="sales";
    public static String  DATABASE_TABLE_SUPPLIERS="suppliers";
    public static String  DATABASE_TABLE_UNITS="units";
    public static String  DATABASE_TABLE_VARIANTATTRIBUTEOPTION="variantAttributeOptions";
    public static String  DATABASE_TABLE_VARIANTATTRIBUTES="variantAttributes";
    public static String  DATABASE_TABLE_VOUCHERS="vouchers";

    // --end of all table name define--------



}
